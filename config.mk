# Overlay
PRODUCT_PACKAGES += \
    CertResOverlay

PRODUCT_COPY_FILES += \
    vendor/aosp/certs/config/config-system_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/overlay/config/config.xml
